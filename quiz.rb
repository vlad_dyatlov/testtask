require 'mechanize'
require 'nokogiri'

class Quiz
  def run

    @results = {}
    temp_array = []

    mech = Mechanize.new
    login = mech.get('https://staqresults.herokuapp.com')
    form = login.forms.first
    form['email'] = 'test@example.com'
    form['password'] = 'secret'
    login = form.submit

    html = login.body
    doc = Nokogiri::HTML(html, nil, 'UTF-8')

    rows = doc.css('tr')
    column_names = rows.shift.css('th').map(&:text)
    column_names = column_names.map  {|el| el.downcase.to_sym}

    text_all_rows = rows.map do |row|
      row_values = row.css('td').map(&:text)
      array = [*row_values]
      arr = []
      arr = arr.push(array[0])
      temp_results = arr.map  {|key| [key.to_s, column_names[1..-1].zip(array[1..-1]).to_h] }.to_h
      temp_array << temp_results
    end
    @results = temp_array.reduce(Hash.new, :merge)
    @results

  end
end